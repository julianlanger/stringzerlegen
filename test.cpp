#include <iostream>
#include <fstream>
#include <string>
#include <streambuf>
#include <vector>

using namespace std;

string getFileContent(string filename)
{
	filename = filename.length() > 0 ? filename : "input.txt";
	ifstream file(filename);
	string str((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());

	return str;
}

string parseValue(const char* &input_ptr)
{
	string value = "";

	// Leerstellen und Zeilenumbrüche vor Values entfernen
	while (*input_ptr == ' ' || *input_ptr == '\n') {
		input_ptr++;
	}

	if (*input_ptr == '(') {
		input_ptr++;

		// führende Leerstellen in Values überspringen:
		while (*input_ptr == ' ') {
			input_ptr++;
		}

		while (*input_ptr != ')') {
			if (*input_ptr != '\n') {
				value += *input_ptr;
			}
			input_ptr++;
		}
		if (*input_ptr != ')') {
			throw invalid_argument("missing closing ')'");
		}
		input_ptr++;
	} else if (*input_ptr == 'n') { // Fall value ist "null" behandeln
		input_ptr += 4;
		value = "null";
	} else if (*input_ptr == '/') { // Fall value ist "/Off" behandeln
		input_ptr++;
		if (*input_ptr == 'O') {
			input_ptr += 2;
			value = "Off";
		}
	} else {
		throw invalid_argument("invalid Data");
	}

	return value;
}

string parseTag(const char* &input_ptr)
{
	if (*input_ptr != '<') {
		throw invalid_argument("second '<' missing");
	}
	input_ptr++;

	string tag;

	while (*input_ptr != '>') {
		if (*input_ptr == '/') {
			input_ptr++;
			if (*input_ptr == 'T' || *input_ptr == 'V') {
				bool isName = *input_ptr == 'T';
				input_ptr++;
				tag += parseValue(input_ptr);
				tag += isName ? ": " : "";
			}
		} else {
			input_ptr++;
		}
	}

	input_ptr++;
	if (*input_ptr != '>') {
		throw invalid_argument("second '>' missing");
	}
	input_ptr++;

	return tag;
}

vector<string> parseDocument(const char* input_ptr)
{
	vector<string> parsedTags;

	// Dateiinformationen wegschneiden
	while (*input_ptr != '[') {
		input_ptr++;
	}
	
	while(*input_ptr != ']') {
		if (*input_ptr == '<') {
			input_ptr++;
			parsedTags.emplace_back(parseTag(input_ptr));
		} else {
			input_ptr++;
		}
	}

	return parsedTags;
}

int main() {
	string filename;
	cout << "Enter full path of file to read (leave empty for default input.txt): ";
	getline(cin, filename);
	const string input = getFileContent(filename);
	const char* input_ptr = input.c_str();
	
	for(const string &tag : parseDocument(input_ptr)) {
		cout << tag << "\n";
	}

	return 0;
}
